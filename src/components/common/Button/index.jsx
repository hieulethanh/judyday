import React from "react";

import "./style.css";

const Button = (props) => {
  const { children, disabled, onClick, className } = props;
  return (
    <button
      onClick={!disabled ? onClick : () => {}}
      className={`btn first ${className}`}
    >
      {children}
    </button>
  );
};

export default Button;
