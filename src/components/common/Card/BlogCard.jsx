import React from "react";
import { Card } from "antd";

import "./style.css";

const { Meta } = Card;

const BlogCard = (props) => {
  const { src, title, description, name } = props;
  const gridStyle = {
    textAlign: "center",
  };

  return (
    <Card
      hoverable
      bordered={false}
      style={{ width: "100%" }}
      cover={<img alt="hinh" style={{ height: 400 }} src={src} />}
    >
      <Meta style={gridStyle} title={title} description={description} />
      <p style={gridStyle}>{name}</p>
    </Card>
  );
};

export default BlogCard;
