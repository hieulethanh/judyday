import React, { useEffect, useState } from "react";
import axios from "axios";
import { Col, Row } from "antd";
import BlogCard from "../../common/Card/BlogCard";
import "./index.css";

function TravelPage() {
  const [blogs, setBlogs] = useState([]);

  useEffect(() => {
    axios
      .get("https://judybyday.herokuapp.com/api/blog/get-travel")
      .then((response) => {
        if (response.data.success) {
          setBlogs(response.data.blogs);
        } else {
          alert("Couldnt get blog`s lists");
        }
      });
  }, []);
  console.log(blogs);
  const renderCards = blogs.map((blog, index) => {
    return (
      <Col
        xs={{ span: 24 }}
        sm={{ span: 12 }}
        md={{ span: 12 }}
        lg={{ span: 8 }}
        xl={{ span: 8 }}
      >
        <a href={`/blog/post/${blog._id}`}>
          <BlogCard
            src={blog.url}
            description={blog.description}
            title={blog.title}
            name={blog.writer.name}
          />
        </a>
      </Col>
    );
  });

  return (
    <div style={{ width: "85%", margin: "3rem auto" }}>
      <Row gutter={[32, 16]}>{renderCards}</Row>
    </div>
  );
}

export default TravelPage;
