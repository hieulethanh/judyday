import React, { useState } from "react";

import QuillEditor from "../../../editor/QuillEditor";
import { Typography, Button, Form, message, Input } from "antd";
import axios from "axios";
import { useSelector } from "react-redux";
import TextArea from "antd/lib/input/TextArea";

const { Title } = Typography;

function CreatePage(props) {
  const user = useSelector((state) => state.user);

  const [content, setContent] = useState("");
  const [state, setState] = useState({
    title: "",
    category: "",
    description: "",
  });

  const [img, setImg] = useState(null);

  const onImgChange = (event) => {
    setImg(event.target.files[0]);
  };
  // eslint-disable-next-line
  const [files, setFiles] = useState([]);

  const onEditorChange = (value) => {
    setContent(value);
  };

  const onFilesChange = (files) => {
    setFiles(files);
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const onSubmit = async (event) => {
    event.preventDefault();
    const formData = new FormData();
    formData.append("file", img);
    formData.append("content", content);
    formData.append("userID", user.userData._id);
    formData.append("title", state.title);
    formData.append("category", state.category);
    formData.append("description", state.description);
    const config = {
      header: { "content-type": "multipart/form-data" },
    };

    if (user.userData && !user.userData.isAuth) {
      return alert("Please Log in first");
    }

    await axios
      .post("/api/blog/createPost", formData, config)
      .then((response) => {
        if (response) {
          message.success("Post Created!");

          setTimeout(() => {
            props.history.push("/blog");
          }, 2000);
        }
      });
  };

  return (
    <div style={{ maxWidth: "700px", margin: "2rem auto" }}>
      <div style={{ textAlign: "center" }}>
        <Title level={2}> Editor</Title>
      </div>
      <Input
        type="text"
        name="title"
        value={state.title}
        placeholder="Text your blog title..."
        onChange={handleChange}
        style={{ margin: 10 }}
      />
      <TextArea
        type="text"
        name="description"
        value={state.description}
        placeholder="Text your blog description..."
        onChange={handleChange}
        style={{ margin: 10 }}
      />
      <input type="file" name="file" onChange={onImgChange} />
      <QuillEditor
        placeholder={"Start Posting Something"}
        onEditorChange={onEditorChange}
        onFilesChange={onFilesChange}
      />
      <label>Category</label>
      <select
        style={{ margin: 10, width: 300 }}
        name="category"
        onChange={handleChange}
      >
        <option value=""></option>
        <option value="travel">Travel</option>
        <option value="cooking">Cooking</option>
        <option value="business">Business</option>
        <option value="style">My style</option>
      </select>
      <Form onSubmit={onSubmit}>
        <div style={{ textAlign: "center", margin: "2rem" }}>
          <Button size="large" htmlType="submit" onSubmit={onSubmit}>
            Submit
          </Button>
        </div>
      </Form>
    </div>
  );
}

export default CreatePage;
