import React, { useState, useEffect } from "react";
import Button from "../../common/Button";
import axios from "axios";
import { Col, Row } from "antd";
import BlogCard from "../../common/Card/BlogCard";
import "./style.css";

function LandingPage() {
  const [travel, setTravel] = useState([]);
  const [cooking, setCooking] = useState([]);
  const [business, setBusiness] = useState([]);
  const [style, setStyle] = useState([]);

  useEffect(() => {
    axios.get("/api/blog/get-travel").then((response) => {
      if (response.data.success) {
        setTravel(response.data.blogs);
      } else {
        alert("Couldnt get blog`s lists");
      }
    });
    axios.get("/api/blog/get-cooking").then((response) => {
      if (response.data.success) {
        setCooking(response.data.blogs);
      } else {
        alert("Couldnt get blog`s lists");
      }
    });
    axios.get("/api/blog/get-business").then((response) => {
      if (response.data.success) {
        setBusiness(response.data.blogs);
      } else {
        alert("Couldnt get blog`s lists");
      }
    });
    axios.get("/api/blog/get-style").then((response) => {
      if (response.data.success) {
        setStyle(response.data.blogs);
      } else {
        alert("Couldnt get blog`s lists");
      }
    });
  }, []);

  const renderCards = travel.slice(0, 3).map((blog, index) => {
    return (
      <Col
        xs={{ span: 24 }}
        sm={{ span: 12 }}
        md={{ span: 12 }}
        lg={{ span: 8 }}
        xl={{ span: 8 }}
        key={index}
      >
        <a href={`/blog/post/${blog._id}`}>
          <BlogCard
            src={blog.url}
            description={blog.description}
            name={blog.writer.name}
            title={blog.title}
          />
        </a>
      </Col>
    );
  });
  const renderCards1 = cooking.slice(0, 3).map((blog, index) => {
    return (
      <Col
        xs={{ span: 24 }}
        sm={{ span: 12 }}
        md={{ span: 12 }}
        lg={{ span: 8 }}
        xl={{ span: 8 }}
        key={index}
      >
        <a href={`/blog/post/${blog._id}`}>
          <BlogCard
            src={blog.url}
            description={blog.description}
            name={blog.writer.name}
            title={blog.title}
          />
        </a>
      </Col>
    );
  });
  const renderCards2 = business.slice(0, 3).map((blog, index) => {
    return (
      <Col
        xs={{ span: 24 }}
        sm={{ span: 12 }}
        md={{ span: 12 }}
        lg={{ span: 8 }}
        xl={{ span: 8 }}
        key={index}
      >
        <a href={`/blog/post/${blog._id}`}>
          <BlogCard
            src={blog.url}
            description={blog.description}
            name={blog.writer.name}
            title={blog.title}
          />
        </a>
      </Col>
    );
  });
  const renderCards3 = style.slice(0, 3).map((blog, index) => {
    return (
      <Col
        xs={{ span: 24 }}
        sm={{ span: 12 }}
        md={{ span: 12 }}
        lg={{ span: 8 }}
        xl={{ span: 8 }}
        key={index}
      >
        <a href={`/blog/post/${blog._id}`}>
          <BlogCard
            src={blog.url}
            description={blog.description}
            name={blog.writer.name}
            title={blog.title}
          />
        </a>
      </Col>
    );
  });

  return (
    <>
      <div className="app"></div>
      <div className="btn-about">
        <Button onClick={(event) => (window.location.href = "/about")}>
          More about me
        </Button>
      </div>
      <div className="intro-about">
        <p className="text-about">
          {" "}
          Có người từng nói “Hạnh phúc lớn nhất đơn giản chỉ là thời khắc hiện
          tại bạn thực sự hài lòng với chính những gì bạn có.” Mình đã từng đi
          kiếm tìm hạnh phúc ở tận đâu đâu, mà không biết rằng hạnh phúc nó ở
          ngay những thứ bé nhỏ nhất. Mình đã từng bất mãn với gia đình, đã từng
          tưởng tượng rằng nếu mình sinh ra ở một gia đình khác thì tốt biết
          bao. Mình đã từng cố gắng thay đổi người mình yêu trở thành một người
          hoàn hảo. Mình cũng đã từng theo đuổi một lý tưởng “vô thực”, cứ tưởng
          như con người này, cuộc sống này không thuộc về mình vậy. Và rồi những
          ý nghĩ tiêu cực đó cứ vồ lấy mình hàng ngày, hàng đêm. Mình rơi vào
          cơn trầm cảm lúc nào không hay.
        </p>
      </div>
      <div className="first-img">
        <div className="bg-first"></div>
        <div className="text-first">
          <p>"We take photos as a return ticket to a moment otherwise gone"</p>
        </div>
      </div>
      <div className="btn-about">
        <Button onClick={(event) => (window.location.href = "/travel")}>
          Travel
        </Button>
      </div>
      <div style={{ width: "90%", margin: "3rem auto" }}>
        <Row gutter={[16, 16]}>{renderCards}</Row>
      </div>
      <div className="first-img">
        <div className="bg-second"></div>
        <div className="text-first">
          <p>
            "A recipe has no soul. You as the cook must bring soul to the
            recipe."
          </p>
        </div>
      </div>
      <div className="btn-about">
        <Button onClick={(event) => (window.location.href = "/cooking")}>
          Cooking
        </Button>
      </div>
      <div style={{ width: "90%", margin: "3rem auto" }}>
        <Row gutter={[16, 16]}>{renderCards1}</Row>
      </div>
      <div className="first-img">
        <div className="bg-third"></div>
        <div className="text-first">
          <p>"Every problem is a gift—without problems we would not grow."</p>
        </div>
      </div>
      <div className="btn-about">
        <Button onClick={(event) => (window.location.href = "/business")}>
          My Small Business
        </Button>
      </div>
      <div style={{ width: "90%", margin: "3rem auto" }}>
        <Row gutter={[16, 16]}>{renderCards2}</Row>
      </div>
      <div className="first-img">
        <div className="bg-4th"></div>
        <div className="text-first">
          <p>
            "If life were predictable it would cease to be life, and be without
            flavor."
          </p>
        </div>
      </div>
      <div className="btn-about">
        <Button onClick={(event) => (window.location.href = "/style")}>
          My style
        </Button>
      </div>
      <div style={{ width: "90%", margin: "3rem auto" }}>
        <Row gutter={[16, 16]}>{renderCards3}</Row>
      </div>
    </>
  );
}

export default LandingPage;
