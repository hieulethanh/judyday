import React, { useEffect, useState } from "react";
import axios from "axios";
import { Typography } from "antd";
const { Title } = Typography;

function PostPage(props) {
  const [post, setPost] = useState([]);
  const postId = props.match.params.postId;

  useEffect(() => {
    axios.get(`/api/blog/getPost/${postId}`).then((response) => {
      if (response.data.success) {
        setPost(response.data.post);
      } else {
        alert("Couldnt get post");
      }
    });
    // eslint-disable-next-line
  }, []);

  if (post.writer) {
    return (
      <div className="postPage" style={{ width: "80%", margin: "3rem auto" }}>
        <br />
        <div style={{ display: "flex", justifyContent: "center" }}>
          <Title style={{ fontSize: "80px", fontWeight: 400 }} level={6}>
            {post.title}
          </Title>
        </div>
        <div
          style={{ fontSize: "1.5rem" }}
          dangerouslySetInnerHTML={{ __html: post.content }}
        />
      </div>
    );
  } else {
    return <div style={{ width: "80%", margin: "3rem auto" }}>loading...</div>;
  }
}

export default PostPage;
