import React from "react";
import { Menu } from "antd";
import { withRouter } from "react-router-dom";
import { useSelector } from "react-redux";

function LeftMenu(props) {
  const user = useSelector((state) => state.user);
  if (user.userData && !user.userData.isAuth) {
    return (
      <Menu mode={props.mode}>
        <Menu.Item key="mail">
          <a href="/">HOME</a>
        </Menu.Item>
        <Menu.Item key="about">
          <a href="/about">ABOUT</a>
        </Menu.Item>
        <Menu.Item key="blog">
          <a href="/blog">BLOG</a>
        </Menu.Item>
        <Menu.Item key="cooking">
          <a href="/cooking">COOKING</a>
        </Menu.Item>
        <Menu.Item key="travel">
          <a href="/travel">TRAVEL</a>
        </Menu.Item>
        <Menu.Item key="business">
          <a href="/business">MY SMALL BUSINESS</a>
        </Menu.Item>
        <Menu.Item key="style">
          <a href="/style">MY STYLE</a>
        </Menu.Item>
      </Menu>
    );
  } else {
    return (
      <Menu mode={props.mode}>
        <Menu.Item key="mail">
          <a href="/">HOME</a>
        </Menu.Item>
        <Menu.Item key="blog">
          <a href="/blog">BLOG</a>
        </Menu.Item>
        <Menu.Item key="create">
          <a href="/blog/create">CREATE</a>
        </Menu.Item>
      </Menu>
    );
  }
}

export default withRouter(LeftMenu);
